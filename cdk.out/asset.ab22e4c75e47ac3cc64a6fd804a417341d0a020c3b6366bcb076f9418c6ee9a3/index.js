var Jimp = require("jimp");
const AWS = require("aws-sdk");
const S3 = new AWS.S3();

exports.handler = async function (event, context) {
  const { Records } = event;

  try {
    const promArray = Records.map((record) => {
      const bucket = record.s3.bucket.name;
      const file = record.s3.bucket.key;
      const width = 300;
      const height = 300;
      return resizeImage({ bucket, file, width, height });
    });

    await Promise.all(promArray);

    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify({ message: "Successfully uploaded file to S3" }),
    };
  } catch (error) {
    var body = error.stack || JSON.stringify(error, null, 2);
    return {
      statusCode: 400,
      headers: {},
      body: body,
    };
  }
};

const resizeImage = async ({ bucket, file, width, height }) => {
  const imageBuffer = await S3.get(file, bucket);
  const jimpImage = await Jimp.read(imageBuffer.Body);
  const mime = jimpImage.getMIME();
  const ext = mime.split("/")[1];

  const resizedImageBuffer = await jimpImage
    .scaleToFit(width, height)
    .getBufferAsync(mime);

  const shortFileName = file.split("/")[1];
  const newFileName = `resized/${width}x${height}/${shortFileName}.${ext}`;

  await S3.putObject({
    Bucket: bucket,
    Key: newFileName,
    Body: resizedImageBuffer,
    ContentEncoding: "base64",
    ContentType: mime,
  }).promise();

  return newFileName;
};
