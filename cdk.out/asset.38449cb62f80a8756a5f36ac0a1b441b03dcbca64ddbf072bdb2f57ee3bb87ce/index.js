const { v4: uuidv4 } = require("uuid");
const AWS = require("aws-sdk");
const S3 = new AWS.S3();

const bucketName = process.env.BUCKET;
const region = process.env.REGION;

exports.handler = async function (event, context) {
  try {
    var method = event.httpMethod;
    var widgetName = event.path.startsWith("/")
      ? event.path.substring(1)
      : event.path;

    if (method === "GET") {
      // GET / to get the names of all widgets
      if (event.path === "/") {
        const data = await S3.listObjectsV2({ Bucket: bucketName }).promise();
        var body = {
          widgets: data.Contents.map(function (e) {
            return e.Key;
          }),
        };
        return {
          statusCode: 200,
          headers: {},
          body: JSON.stringify(body),
        };
      }

      if (widgetName) {
        // GET /name to get info on widget name
        const data = await S3.getObject({
          Bucket: bucketName,
          Key: widgetName,
        }).promise();
        var body = data.Body.toString("utf-8");

        return {
          statusCode: 200,
          headers: {},
          body: JSON.stringify(body),
        };
      }
    }

    if (method === "POST") {
      const { imageData, mime } = JSON.parse(event.body);

      var base64data = Buffer.from(imageData, "base64");
      const key = uuidv4();

      await S3.putObject({
        Bucket: bucketName,
        Key: key,
        Body: base64data,
        ContentType: mime,
        ACL: "public-read",
      }).promise();

      const url = `https://${bucketName}.s3-${region}.amazonaws.com/${key}.${mime}`;

      return {
        statusCode: 200,
        headers: {},
        body: JSON.stringify(url),
      };
    }

    return {
      statusCode: 400,
      headers: {},
      body: "We only accept GET and POST /",
    };
  } catch (error) {
    var body = error.stack || JSON.stringify(error, null, 2);
    return {
      statusCode: 400,
      headers: {},
      body: body,
    };
  }
};
