import * as s3 from "aws-cdk-lib/aws-s3";
import { v4 as uuid } from "uuid";

exports.handle = async (event, context) => {
  let result = {};

  const { image, mime } = JSON.parse(event.body);

  const name = uuid();
  const body = Buffer.from(image, "base64");

  const params = {
    Bucket: process.env.BUCKET,
    Key: `uploads/${name}`,
    Body: body,
    ContentEncoding: "base64",
    ContentType: mime,
  };

  await s3.putObject(params).promise();

  const url = `https://${process.env.BUCKET}.s3-${process.env.REGION}.amazonaws.com/uploads/${name}`;

  result = { url };

  return {
    statusCode: 200,
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Methods": "*",
      "Access-Control-Allow-Origin": "*",
    },
    body: JSON.stringify(result),
  };
};
