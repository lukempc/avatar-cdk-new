const AWS = require("aws-sdk");
const S3 = new AWS.S3();

const bucketName = process.env.BUCKET;
const region = process.env.REGION;

exports.handler = async function (event, context) {
  try {
    var method = event.httpMethod;
    var widgetName = event.path.startsWith("/")
      ? event.path.substring(1)
      : event.path;

    if (method === "GET") {
      // GET / to get the names of all widgets
      if (event.path === "/") {
        const data = await S3.listObjectsV2({ Bucket: bucketName }).promise();
        var body = {
          widgets: data.Contents.map(function (e) {
            return e.Key;
          }),
        };
        return {
          statusCode: 200,
          headers: {},
          body: JSON.stringify(body),
        };
      }

      if (widgetName) {
        // GET /name to get info on widget name
        const data = await S3.getObject({
          Bucket: bucketName,
          Key: widgetName,
        }).promise();
        var body = data.Body.toString("utf-8");

        return {
          statusCode: 200,
          headers: {},
          body: JSON.stringify(body),
        };
      }
    }

    if (method === "POST") {
      if (event.path === "/") {
        const now = new Date();
        var data = widgetName + " created: " + now;
        const { image } = JSON.parse(event.body);

        const key =
          Math.random().toString(36).substring(2, 10) +
          Math.random().toString(36).substring(2, 10);

        const parts = image.split(";");
        const mime = parts[0].split(":")[1];
        const ext = mime.split("/")[1];

        var body = new Buffer.from(
          image.replace(/^data:image\/\w+;base64,/, ""),
          "base64"
        );

        const result = await S3.putObject({
          Bucket: bucketName,
          Key: `photos/${key}.${ext}`,
          Body: body,
          ContentEncoding: "base64",
          ContentType: mime,
        }).promise();

        const url = `https://cdkstack-homeplusavatar2370a12d-roxoybyum2al.s3.amazonaws.com/photos/${key}.${ext}`;

        return {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
          },
          body: JSON.stringify(url),
        };
      }
    }

    return {
      statusCode: 400,
      headers: {},
      body: "We only accept GET and POST /",
    };
  } catch (error) {
    var body = error.stack || JSON.stringify(error, null, 2);
    return {
      statusCode: 400,
      headers: {},
      body: body,
    };
  }
};
