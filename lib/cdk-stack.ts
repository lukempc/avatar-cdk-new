import { Construct } from "constructs";
import * as apigateway from "aws-cdk-lib/aws-apigateway";
import * as lambda from "aws-cdk-lib/aws-lambda";
import * as s3 from "aws-cdk-lib/aws-s3";
import * as s3n from "aws-cdk-lib/aws-s3-notifications";
import { Stack, StackProps } from "aws-cdk-lib";

export class CdkStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const bucket = new s3.Bucket(this, "homeplus-avatar", {
      publicReadAccess: true,
    });

    const handler = new lambda.Function(this, "avatar-upload", {
      runtime: lambda.Runtime.NODEJS_14_X,
      code: lambda.Code.fromAsset("src/create"),
      handler: "index.handler",
      environment: {
        BUCKET: bucket.bucketName,
      },
    });

    bucket.grantReadWrite(handler);

    // const Resizehandler = new lambda.Function(this, "avatar-resize", {
    //   runtime: lambda.Runtime.NODEJS_14_X,
    //   code: lambda.Code.fromAsset("src/resize"),
    //   handler: "index.handler",
    //   environment: {
    //     BUCKET: bucket.bucketName,
    //   },
    // });

    // bucket.addEventNotification(
    //   s3.EventType.OBJECT_CREATED,
    //   new s3n.LambdaDestination(Resizehandler)
    // );

    const api = new apigateway.RestApi(this, "homeplus-avatar-api", {
      restApiName: "Avatar Service",
      description: "This service serves avatar upload.",
      defaultCorsPreflightOptions: {
        allowHeaders: [
          "Content-Type",
          "X-Amz-Date",
          "Authorization",
          "X-Api-Key",
        ],
        allowMethods: ["OPTIONS", "GET", "POST", "PUT", "PATCH", "DELETE"],
        allowCredentials: true,
        allowOrigins: ["*"],
      },
    });

    const widget = api.root.addResource("{id}");

    const getWidgetIntegration = new apigateway.LambdaIntegration(handler);

    const uploadAvatarIntegration = new apigateway.LambdaIntegration(handler, {
      requestTemplates: { "application/json": '{ "statusCode": "200" }' },
    });

    widget.addMethod("POST", uploadAvatarIntegration); // POST /
    widget.addMethod("GET", getWidgetIntegration); // GET /{id}
  }
}
